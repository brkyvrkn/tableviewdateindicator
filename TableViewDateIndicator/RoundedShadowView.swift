//
//  RoundedShadowView.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import UIKit

class RoundedShadowView: UIView {

    var cornerRadi: CGFloat
    var shadowLayer: CAShapeLayer!
    
    override init(frame: CGRect) {
        let bundleFile = Bundle.main.url(forResource: "Preferences", withExtension: "plist")
        let preferences = NSMutableDictionary(contentsOf: bundleFile!)
        cornerRadi = preferences!["cornerRadiusSmooth"] as! CGFloat
        
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        let bundleFile = Bundle.main.url(forResource: "Preferences", withExtension: "plist")
        let preferences = NSMutableDictionary(contentsOf: bundleFile!)
        cornerRadi = preferences!["cornerRadiusSmooth"] as! CGFloat
        
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let shadow = layer.sublayers?.first(where: { $0.name == "ShadowLayer" }) {
            shadow.removeFromSuperlayer()
            shadowLayer = nil
        }
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadi).cgPath
            shadowLayer.fillColor = backgroundColor?.cgColor

            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = cornerRadi
            shadowLayer.name = "ShadowLayer"

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }

}
