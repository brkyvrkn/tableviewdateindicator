//
//  ScrollIndicatorDateView.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import UIKit

class ScrollIndicatorDateView: UIView, InstantiatableUIView {
    
    //MARK: - Outlets
    @IBOutlet var contentView: RoundedShadowView!

    @IBOutlet weak var indicatorIconImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var indicatorBarView: UIView!
    
    //MARK: - Properties
    private var date_: Date?
    var date: Date? {
        get {
            return date_
        } set {
            if let d = newValue {
                date_ = newValue
                UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                    self.dateLabel.text = self.customFormat.string(from: d)
                    self.alpha = 0.0
                }, completion: nil)
            } else {
                dateLabel.text = "-"
            }
        }
    }
    var customFormat: DateFormatter {
        get {
            let f = DateFormatter()
            f.dateFormat = "MMM dd, YYYY"
            return f
        }
    }
    
    private var shadowLayer: CAShapeLayer!
    private var cornerRadius: CGFloat = 25.0
    private var fillColor: UIColor = .white
    
    
    //MARK: - Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    func commonInit() {
        try? configureView()
        
        contentView.layer.cornerRadius = contentView.bounds.size.height / 2
        contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        
        indicatorBarView.layer.cornerRadius = indicatorBarView.bounds.size.width / 2
        indicatorBarView.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
          
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.bounds.size.height / 2).cgPath
            shadowLayer.fillColor = fillColor.cgColor

            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 10.0)
            shadowLayer.shadowOpacity = 0.3
            shadowLayer.shadowRadius = 99

            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    

}
