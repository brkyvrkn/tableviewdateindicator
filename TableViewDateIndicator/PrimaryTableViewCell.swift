//
//  PrimaryTableViewCell.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import UIKit

class PrimaryTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var roundedShadowView: RoundedShadowView!
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var cellSubtitleLabel: UILabel!
    @IBOutlet weak var cellDateLabel: UILabel!
    
    
    //MARK: - Properties
    static let identifier = "PrimaryTableViewCell"
    static let nibName = "PrimaryTableViewCell"
    
    var itemDate: Date?
    
    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellImageView.layer.cornerRadius = 4
        cellImageView.clipsToBounds = true
    }
    
    func configureCell(_ model: PrimaryTableViewCellModel) {
        cellTitleLabel.text = model.title.capitalized
        cellSubtitleLabel.text = model.subtitle
        itemDate = model.date
        
        let customFormat = DateFormatter()
        customFormat.dateFormat = "MMM dd, YYYY"
        cellDateLabel.text = customFormat.string(from: model.date)
        cellImageView.backgroundColor = UIColor.cyan
    }
    
}
