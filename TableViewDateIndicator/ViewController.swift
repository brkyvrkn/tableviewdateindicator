//
//  ViewController.swift
//  
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    
    
    //MARK: - Properties
    var cells = [PrimaryTableViewCellModel]()
    var topDate: Date? {
        get {
            if mainTableView != nil {
                if let visibleTop = mainTableView.visibleCells.first as? PrimaryTableViewCell {
                    return visibleTop.itemDate
                }
            }
            
            return nil
        }
    }
    
    // Indicator attributes
    var indicator: ScrollIndicatorDateView?
    
    var indicatorColor: UIColor = .lightGray
    var indicatorWidth: CGFloat = 144
    var indicatorHeight: CGFloat = 48
    var indicatorPadding: CGFloat = 5
    var indicatorShowDuration: Double = 0.5
    
    var isFading: Bool = false
    
    var indicatorOffsetMultiplier: CGFloat = 1.0
    
    
    //MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }

    func initView() {
        fetchData()
        initTableView(mainTableView)
        
        mainTableView.reloadSections(IndexSet(integersIn: 0..<mainTableView.numberOfSections), with: .fade)
        indicatorOffsetMultiplier = mainTableView.bounds.size.height / mainTableView.contentSize.height
    }
    
    func fetchData() {
        self.cells = [.d1, .d2, .d3, .d4, .d5, .d6, .d7, .d8, .d9, .d10, .d11, .d12, .d13, .d14, .d15, .d16, .d17, .d18]
        self.cells.sort(by: { (prev, next) in
            prev.date < next.date
        })
    }
    
    func initTableView(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorColor = .clear
        tableView.allowsSelection = false
        
        tableView.register(UINib(nibName: PrimaryTableViewCell.nibName, bundle: .main), forCellReuseIdentifier: PrimaryTableViewCell.identifier)
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        // Required to use custom indicator
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
        tableView.layoutIfNeeded()
        
        // Setup indicator
        showIndicator(in: view, at: .init(x: tableView.frame.maxX - indicatorWidth, y: tableView.frame.origin.y + indicatorPadding))
        indicator?.alpha = 0
    }
    
    
    //MARK: - Custom Indicator Helper
    func showIndicator(in view: UIView, at origin: CGPoint) {
        if let i = view.viewWithTag(15) as? ScrollIndicatorDateView {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.2, options: .curveEaseIn, animations: {
                i.alpha += 0.05
            }, completion: { (completed) in
                i.alpha = 1
            })
            
            i.layoutIfNeeded()
        } else {
            indicator = ScrollIndicatorDateView(frame: .init(x: origin.x,
                                                             y: origin.y,
                                                             width: indicatorWidth,
                                                             height: indicatorHeight))
            indicator?.tag = 15
            view.addSubview(indicator!)
            
            indicator?.translatesAutoresizingMaskIntoConstraints = false
            let _ = NSLayoutConstraint(item: indicator!, attribute: .top, relatedBy: .equal, toItem: mainTableView, attribute: .top, multiplier: 1, constant: 0).isActive = true
            let _ = NSLayoutConstraint(item: indicator!, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -indicatorPadding).isActive = true
            let _ = NSLayoutConstraint(item: indicator!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: indicatorWidth).isActive = true
            let _ = NSLayoutConstraint(item: indicator!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: indicatorHeight).isActive = true
            
            if let initialCell = mainTableView.visibleCells.first as? PrimaryTableViewCell {
                indicator?.date = initialCell.itemDate
            }
        }
    }
    
    func hideIndicator() {
        if let i = view.viewWithTag(15) as? ScrollIndicatorDateView {
            UIView.animate(withDuration: 0.5, animations: {
                i.alpha = 0
            }, completion: nil)
        }
    }
    
    func removeIndicator() {
        if let i = view.viewWithTag(15) as? ScrollIndicatorDateView {
            i.removeFromSuperview()
        }
    }
    
}


//MARK: - Table View Delegate
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PrimaryTableViewCell.identifier, for: indexPath) as? PrimaryTableViewCell else {
            return tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        }
        let item = cells[indexPath.row]
        cell.configureCell(item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}


//MARK: - Scroll View
extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let in_ = indicator, let _ = scrollView as? UITableView else { return }
        indicatorOffsetMultiplier = scrollView.bounds.size.height / scrollView.contentSize.height
        
        var new = in_.frame
        new.origin.y = scrollView.frame.origin.y + (scrollView.contentOffset.y * indicatorOffsetMultiplier * 1.31)   // 1.31 is heuristic
        
        guard let visibleMin = topDate else { return }
        if in_.date != topDate {
            in_.date = visibleMin
            showIndicator(in: view, at: new.origin)
            return
        }
        indicator?.frame = new
        showIndicator(in: view, at: new.origin)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.hideIndicator()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.hideIndicator()
        }
    }
    
}
