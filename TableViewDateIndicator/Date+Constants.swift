//
//  Date+Constants.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import Foundation


extension Date {
    
    static let today: Date = {
        return Date()
    }()
    
    
    // Future
    static let tomorrow: Date = {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
    }()
    
    static let aWeekLater: Date = {
        return Calendar.current.date(byAdding: .weekOfYear, value: 1, to: Date())!
    }()
    
    static let aMonthLater: Date = {
        return Calendar.current.date(byAdding: .month, value: 1, to: Date())!
    }()
    
    static let aYearLayer: Date = {
        return Calendar.current.date(byAdding: .year, value: 1, to: Date())!
    }()
    
    // Past
    static let yesterday: Date = {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date())!
    }()
    
    static let aWeekAgo: Date = {
        return Calendar.current.date(byAdding: .weekOfYear, value: -1, to: Date())!
    }()
    
    static let aMonthAgo: Date = {
        return Calendar.current.date(byAdding: .month, value: -1, to: Date())!
    }()
    
}


// MARK: - Helper
extension Date {
    
    /// For example, *random(from: 2, to: 3)* will generate the date
    /// from 2 years ago to 3 years later
    ///
    /// - Parameter past: Lower bound
    /// - Parameter future: Upper bound
    static func random(from past: Int, to future: Int) -> Date {
        let before = Calendar.current.date(byAdding: .year, value: -past, to: Date())!
        let after = Calendar.current.date(byAdding: .year, value: future, to: Date())!
        let randIso = Double.random(in: 0...after.timeIntervalSinceReferenceDate - before.timeIntervalSinceReferenceDate)
        
        return before.addingTimeInterval(randIso)
    }
    
}
