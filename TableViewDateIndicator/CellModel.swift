//
//  CellModel.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 12.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import Foundation


struct PrimaryTableViewCellModel {
    
    var imageUrl: String?
    var title: String
    var subtitle: String
    var date: Date
}

extension PrimaryTableViewCellModel {
    
    static let d1 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/1/160/90",
                                              title: "sed",
                                              subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                                              date: .random(from: 1, to: 1))
    
    static let d2 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/2/160/90",
                                              title: "tempus",
                                              subtitle: "Proin semper vulputate eleifend.",
                                              date: .random(from: 1, to: 1))
    
    static let d3 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/3/160/90",
                                              title: "risus",
                                              subtitle: "Nam efficitur dui vitae nisi tincidunt pretium.",
                                              date: .random(from: 1, to: 1))
    
    static let d4 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/4/160/90",
                                              title: "ac",
                                              subtitle: "In eget consectetur dui, vel viverra elit.",
                                              date: .random(from: 1, to: 1))
    
    static let d5 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/5/160/90",
                                              title: "sem",
                                              subtitle: "Aliquam in sapien non purus rhoncus rutrum ut eget mi.",
                                              date: .random(from: 1, to: 1))
    
    static let d6 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/6/160/90",
                                              title: "rhoncus",
                                              subtitle: "Quisque ut felis non arcu malesuada accumsan.",
                                              date: .random(from: 1, to: 1))
    
    static let d7 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/7/160/90",
                                              title: "interdum",
                                              subtitle: "Maecenas non tempus metus. Pellentesque habitant morbi tristique senectus",
                                              date: .random(from: 1, to: 1))
    
    static let d8 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/8/160/90",
                                              title: "nulla",
                                              subtitle: "Duis quis purus non lorem auctor cursus. Ut sed lectus neque.",
                                              date: .random(from: 1, to: 1))
    
    static let d9 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/9/160/90",
                                              title: "maximus",
                                              subtitle: "Phasellus non molestie metus, et dapibus nisi.",
                                              date: .random(from: 1, to: 1))
    
    static let d10 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/10/160/90",
                                              title: "eget",
                                              subtitle: "Vivamus nisl enim, pharetra a metus congue, placerat maximus erat.",
                                              date: .random(from: 1, to: 1))
    
    static let d11 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/11/160/90",
                                              title: "diam",
                                              subtitle: "Pellentesque molestie, mauris eget volutpat egestas, turpis est volutpat lectus",
                                              date: .random(from: 1, to: 1))
    
    static let d12 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/12/160/90",
                                              title: "sed",
                                              subtitle: "Etiam ullamcorper posuere lacinia. Donec vestibulum tincidunt est",
                                              date: .random(from: 1, to: 1))
    
    static let d13 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/13/160/90",
                                              title: "fringilla",
                                              subtitle: "Pellentesque molestie, mauris eget volutpat egestas",
                                              date: .random(from: 1, to: 1))
    
    static let d14 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/14/160/90",
                                              title: "etiam",
                                              subtitle: " turpis est volutpat lectus, nec sodales ligula nisl ac augue.",
                                              date: .random(from: 1, to: 1))
    
    static let d15 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/15/160/90",
                                              title: "ornare",
                                              subtitle: "Etiam ullamcorper posuere lacinia.",
                                              date: .random(from: 1, to: 1))
    
    static let d16 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/16/160/90",
                                              title: "dui",
                                              subtitle: "Donec vestibulum tincidunt est, sit amet congue urna finibus at.",
                                              date: .random(from: 1, to: 1))
    
    static let d17 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/17/160/90",
                                              title: "orci",
                                              subtitle: "Vivamus nisl enim, pharetra a metus congue, placerat maximus erat.",
                                              date: .random(from: 1, to: 1))
    
    static let d18 = PrimaryTableViewCellModel(imageUrl: "https://picsum.photos/id/18/160/90",
                                              title: "mollis",
                                              subtitle: "Pellentesque molestie, mauris eget volutpat egestas, turpis est volutpat lectus",
                                              date: .random(from: 1, to: 1))
}
