//
//  DateFormatter+Formats.swift
//  TableViewDateIndicator
//
//  Created by Berkay Vurkan on 13.03.2020
//  Copyright © 2020 foo. All rights reserved.
//

import Foundation


extension DateFormatter {
    
    static func customFormat() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, YYYY"
        
        return formatter
    }
    
}
